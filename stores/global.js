import { defineStore } from 'pinia'

export const useGlobal = defineStore('main', {
    state: () => {
        return {
            // Type du portfolio actif
            activePortfolioType: 'site-internet',
        }
    },
    actions: {
        /**
            * Méthode permettant de gérer le type de contenu du portfolio
            * @return void
        */
        setActivePortfolioType(portfolioType) {
            this.activePortfolioType = portfolioType;
        }
    },
    getters: {
        /**
            * Méthode permettant de récupérer le type du portfolio
            * @return array
        */
        getActivePortfolioType() {
            return this.activePortfolioType;
        }
    }
})
