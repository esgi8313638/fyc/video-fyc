export default {

    // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
    ssr: true,

    // Target: https://go.nuxtjs.dev/config-target
    target: 'static',

    // Global page headers: https://go.nuxtjs.dev/config-head
    head() {
        return {
            title: 'FYC - Portfolio',

            htmlAttrs: {
                lang: 'fr'
            },

            meta: [
                { charset: 'utf-8' },
                { name: 'viewport', content: 'width=device-width, initial-scale=1' },
                {
                    hid: 'description',
                    name: 'description',
                    content: ''
                }
            ],

            link: [
                { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
            ]
        }
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [
        // SCSS file in the project
        '~/assets/styles/main.scss'
    ],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: [
        '~/components',
        '~/components/blocks',
        '~/components/elements',
        '~/components/layouts'
    ],

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        // https://go.nuxtjs.dev/tailwindcss
        '@nuxtjs/tailwindcss',
        // https://composition-api.nuxtjs.org/getting-started/setup#quick-start
        '@nuxtjs/composition-api/module',
        // https://pinia.vuejs.org/
        '@pinia/nuxt',
        // https://google-fonts.nuxtjs.org/
        '@nuxtjs/google-fonts'
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://i18n.nuxtjs.org/
        '@nuxtjs/i18n'
    ],

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        transpile: [
            'gsap'
        ],
    },

    middleware: [],

    // locales config
    i18n: {
        locales: [
            { code: 'fr', iso: 'fr-FR', file: 'fr.json' }
        ],
        defaultLocale: 'fr',
        langDir: '~/locales/'
    },

    // Google fonts config
    googleFonts: {
        prefetch: true,
        display: 'swap',
        families: {
            Poppins: [100, 200, 300, 400, 500, 600, 700]
        }
    },

    //Disable telemetry
    telemetry: false,

}
